const arrDevelopers = [5, 8, 3,];
const arrTasks = [1, 2, 4, 5,];
const dedline = new Date('February 24, 2023');



let getSumPoint = (arr) => {
    var sum = 0;
    for (i = 0; i < arr.length; i++) {
      sum += arr[i];
    }
    return sum
  }

let calcTime = (arr1, arr2, timeDedline) => {
    let developersPointsOfDay = getSumPoint(arr1);
    let needPoints = getSumPoint(arr2);

    let restOfTime = timeDedline.getDate() - new Date().getDate();
    let timeToFinish = needPoints / developersPointsOfDay;
    
    if (restOfTime > timeToFinish) {
        let rest = Math.round(restOfTime - timeToFinish);
        return `Все задачи будут успешно выполнены за ${rest} дней до наступления дедлайна!`
    }

    if (restOfTime < timeToFinish) {
        let rest = Math.round(timeToFinish - restOfTime);
        let restHours = rest * 8;
        return `Команде разработчиков придется потратить дополнительно ${restHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`
    } 
}


let developerWork = calcTime(arrDevelopers, arrTasks, dedline);
console.log(developerWork);